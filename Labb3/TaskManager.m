//
//  TaskManager.m
//  Labb3
//
//  Created by IT-Högskolan on 2015-02-17.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "TaskManager.h"
#import "Task.h"

@implementation TaskManager

-(NSMutableArray*)tasks{
    if(!_tasks){
        
        Task *item1 = [[Task alloc]init];
        Task *item2 = [[Task alloc]init];
        Task *item3 = [[Task alloc]init];
        Task *item4 = [[Task alloc]init];
        item1.task = @"Eat";
        item2.task = @"Bathe";
        item3.task = @"Clean";
        item4.task = @"Get dressed";
        
        _tasks = [@[item1, item2, item3, item4] mutableCopy];
        
    }
    return _tasks;
}

-(void)addItem:(NSString *)text{
    Task *item = [[Task alloc]init];
    item.task = text;
    [self.tasks addObject:item];
}

-(NSMutableArray*)returArray{
    return [[NSMutableArray alloc] initWithArray: self.tasks];
}
@end