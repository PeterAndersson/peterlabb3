//
//  ThingsToDoTable.m
//  Labb3
//
//  Created by IT-Högskolan on 2015-02-08.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "ThingsToDoTable.h"
#import "Task.h"
#import "TaskManager.h"
#import "AddTaskViewController.h"

@interface ThingsToDoTable()
@property TaskManager *taskManager;
@end

@implementation ThingsToDoTable


- (void)viewDidLoad {
    self.taskManager = [[TaskManager alloc]init];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}



- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath {

    // self.title = [NSString stringWithFormat:@"%i", indexPath.row];
    [self.taskManager.tasks removeObjectAtIndex:indexPath.row];
    [self.tableView reloadData];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.taskManager.tasks.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"PetesCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Task *item = self.taskManager.tasks[indexPath.row];
    cell.textLabel.text = item.task;
    
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    AddTaskViewController *addview = [segue destinationViewController];
    addview.taskManager = self.taskManager;
}

@end
