//
//  AddTaskViewController.h
//  Labb3
//
//  Created by IT-Högskolan on 2015-02-08.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task.h"
#import "TaskManager.h"

@interface AddTaskViewController : UIViewController

@property TaskManager *taskManager;

@end
