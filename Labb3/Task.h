//
//  Task.h
//  Labb3
//
//  Created by IT-Högskolan on 2015-02-09.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Task : NSObject

@property NSString* task;

@end
