//
//  TaskManager.h
//  Labb3
//
//  Created by IT-Högskolan on 2015-02-17.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TaskManager : NSObject
-(NSMutableArray*)returArray;
-(void)addItem:(NSString*) text;
@property (nonatomic, readwrite) NSMutableArray *tasks;
@end
